package com.michaelhyun.a20220418_michaelhyun_nycschools

import org.junit.Assert.*
import org.junit.Test
import java.util.concurrent.CountDownLatch

class TestUtilTest {

    @Test
    fun `no sat data returns false`() {
        val result = TestUtil.schoolHasSatData("dbn123")

        assertEquals(false, result)
    }
}