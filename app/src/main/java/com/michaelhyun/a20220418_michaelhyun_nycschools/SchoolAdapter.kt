package com.michaelhyun.a20220418_michaelhyun_nycschools

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.michaelhyun.a20220418_michaelhyun_nycschools.model.School
import com.michaelhyun.a20220418_michaelhyun_nycschools.view.DetailsActivity

class SchoolAdapter(val context: Context, val schoolList: List<School>) : RecyclerView.Adapter<SchoolAdapter.SchoolViewHolder>() {

    inner class SchoolViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val schoolName = view?.findViewById<TextView>(R.id.tv_schoolname)
        val address = view?.findViewById<TextView>(R.id.tv_location)
        val phone = view?.findViewById<TextView>(R.id.tv_number)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)

        return SchoolViewHolder(view)
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.schoolName.text = schoolList[position].school_name
        holder.address.text = schoolList[position].primary_address_line_1
        holder.phone.text = schoolList[position].phone_number

        // When an item is clicked, it sends dbn and school name to the details activity.
        // This is so dbn can be used in the retrofit query and name goes into the view
        // Additional information can be added here to be displayed in the second activity
        holder.itemView.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, DetailsActivity::class.java)
            intent.putExtra("dbn", schoolList[position].dbn)
            intent.putExtra("schoolname", schoolList[position].school_name)
            intent.putExtra("overview", schoolList[position].overview_paragraph)
            intent.putExtra("website", schoolList[position].website)

            context.startActivity(intent)
        })
    }

    override fun getItemCount(): Int {
        return schoolList.count()
    }
}