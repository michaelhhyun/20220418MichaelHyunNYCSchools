package com.michaelhyun.a20220418_michaelhyun_nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.michaelhyun.a20220418_michaelhyun_nycschools.SchoolInterface
import com.michaelhyun.a20220418_michaelhyun_nycschools.model.Sat
import com.michaelhyun.a20220418_michaelhyun_nycschools.model.School
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SchoolViewModel: ViewModel() {

    val errorMessage = MutableLiveData<String>()

    // Will load schools to schoolList
    val schoolList: MutableLiveData<List<School>> by lazy {
        MutableLiveData<List<School>>().also {
            loadSchools()
        }
    }

    val satScores: MutableLiveData<List<Sat>> by lazy {
        MutableLiveData<List<Sat>>().also {
        }
    }

    // returns list of schools
    fun getSchools(): LiveData<List<School>> {
        return schoolList
    }

    // Runs API call with dbn query and loads into satScores
    fun getSatScores(dbn: String): LiveData<List<Sat>> {
        loadSat(dbn)
        return satScores
    }

    // Function to run API call for schools
    private fun loadSchools() {
        val schoolData = SchoolInterface.getSchoolData().getSchools()

        schoolData.enqueue(object : Callback<List<School>?> {
            override fun onResponse(call: Call<List<School>?>, response: Response<List<School>?>) {
                schoolList.postValue(response.body())
            }

            override fun onFailure(call: Call<List<School>?>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }

    // Function to get SAT scores using dbn query
    private fun loadSat(dbn: String) {
        val satData = SchoolInterface.getSchoolData().getSat(dbn)

        satData.enqueue(object : Callback<List<Sat>?> {
            override fun onResponse(call: Call<List<Sat>?>, response: Response<List<Sat>?>) {
                satScores.postValue(response.body())
            }

            override fun onFailure(call: Call<List<Sat>?>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }
}