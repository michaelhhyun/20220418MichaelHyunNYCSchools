package com.michaelhyun.a20220418_michaelhyun_nycschools.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.michaelhyun.a20220418_michaelhyun_nycschools.R
import com.michaelhyun.a20220418_michaelhyun_nycschools.SchoolAdapter
import com.michaelhyun.a20220418_michaelhyun_nycschools.SchoolInterface
import com.michaelhyun.a20220418_michaelhyun_nycschools.model.Sat
import com.michaelhyun.a20220418_michaelhyun_nycschools.viewmodel.SchoolViewModel
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    lateinit var viewModel: SchoolViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // observes for change in schools and loads into recyclerview
        viewModel = ViewModelProvider(this).get(SchoolViewModel::class.java)
        viewModel.getSchools().observe(this, Observer { it
            rv_recyclerview.apply {
                layoutManager = LinearLayoutManager(this@MainActivity)
                adapter = SchoolAdapter(this@MainActivity, it)
            }
        })
    }
}