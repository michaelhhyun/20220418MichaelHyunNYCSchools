package com.michaelhyun.a20220418_michaelhyun_nycschools.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.michaelhyun.a20220418_michaelhyun_nycschools.R
import com.michaelhyun.a20220418_michaelhyun_nycschools.viewmodel.SchoolViewModel
import kotlinx.android.synthetic.main.activity_details.*
class DetailsActivity : AppCompatActivity() {

    lateinit var viewModel: SchoolViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        // Receives dbn to be used in retrofit call to find SAT scores
        val dbn = intent.getStringExtra("dbn")

        // School name is to be used in the default page in case dbn cannot be found in API
        val name = intent.getStringExtra("schoolname")
        tv_details_schoolname.text = name

        // School overview to be added to default page
        val overview = intent.getStringExtra("overview")
        tv_overview.text = overview

        // Website to be added to default
        val website = intent.getStringExtra("website")
        tv_website.text = website


        // Loads Sat Scores if the school has a dbn in the SAT database otherwise loads unavailable
        viewModel = ViewModelProvider(this).get(SchoolViewModel::class.java)
        if (dbn != null) {
            viewModel.getSatScores(dbn).observe(this, Observer {
                if (!it?.isEmpty()!!) {
                    tv_reading.text =
                        "Critical Reading average is: ${it.get(0).sat_critical_reading_avg_score.toString()}"
                    tv_math.text =
                        "Math average is: ${it.get(0).sat_math_avg_score.toString()}"
                    tv_writing.text =
                        "Writing average is: ${it.get(0).sat_writing_avg_score.toString()}"
                    tv_testtakers.text =
                        "The number of test takers is: ${it.get(0).num_of_sat_test_takers.toString()}"
                }
            })
        }
    }
}