package com.michaelhyun.a20220418_michaelhyun_nycschools

import com.michaelhyun.a20220418_michaelhyun_nycschools.model.Sat
import com.michaelhyun.a20220418_michaelhyun_nycschools.model.School
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

const val BASE_URL = "https://data.cityofnewyork.us/resource/"

interface SchoolInterface {

    // Call to get schools
    @GET("s3k6-pzi2.json")
    fun getSchools(): Call<List<School>>

    // Call to get SAT scores
    @GET("f9bf-2cp4.json?")
    fun getSat(@Query("dbn") dbn: String): Call<List<Sat>>

    companion object {
        fun getSchoolData(): SchoolInterface  {
            var retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            return retrofit.create(SchoolInterface::class.java)
        }
    }
}